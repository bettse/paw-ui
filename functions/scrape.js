Array.prototype.flat = function() {
  return this.reduce((acc, val) => acc.concat(val), []);
};

const scrapeIt = require('scrape-it');
const {InfluxDB, Point} = require('@influxdata/influxdb-client');

const availableUrl =
  'https://parkavewestpdx.securecafe.com/onlineleasing/park-avenue-west/availableunits.aspx';
const DECIMAL = 10;

const headers = {
  'Content-Type': 'application/json',
};

const {
  NETLIFY_DEV,
  INFLUXDB_V2_BUCKET,
  INFLUXDB_V2_TOKEN,
  INFLUXDB_V2_ORG,
  INFLUXDB_V2_URL,
} = process.env;

const writeApi = new InfluxDB({
  url: INFLUXDB_V2_URL,
  token: INFLUXDB_V2_TOKEN,
}).getWriteApi(INFLUXDB_V2_ORG, INFLUXDB_V2_BUCKET, 'ns', {
  writeFailed: console.log,
});
writeApi.useDefaultTags({complex: 'parkavewest', scraper: 'netlify'});

const descriptionRegex = /Unit ([A-Z]) - ([0-2]) Bedrooms?, ([1-2]) Bathrooms?/g;
const parseDescription = field => {
  return description => {
    if (description === '') return undefined;
    try {
      const result = Array.from([...description.matchAll(descriptionRegex)][0]);
      const [all, floorplan, beds, baths] = result;
      return {floorplan, beds, baths}[field];
    } catch (e) {
      console.log('parseDescription error', e);
      return '';
    }
  };
};

const scrapeOptions = {
  available: {
    listItem: '#innerformdiv .row-fluid .block',
    data: {
      floorplan: {
        selector: '.row-fluid .pull-left h3',
        convert: parseDescription('floorplan'),
      },
      beds: {
        selector: '.row-fluid .pull-left h3',
        convert: parseDescription('beds'),
      },
      baths: {
        selector: '.row-fluid .pull-left h3',
        convert: parseDescription('baths'),
      },
      prices: {
        listItem: '.AvailUnitRow',
        data: {
          UnitNumber: {
            selector: '[data-label="Apartment"]',
            convert: x => parseInt(x.replace('#', ''), DECIMAL),
          },
          sqft: {
            selector: '[data-label="Sq.Ft."]',
            convert: x => parseInt(x, DECIMAL),
          },
          min: {
            selector: '[data-label="Rent"]',
            convert: x => parseRent(x, 0),
          },
          max: {
            selector: '[data-label="Rent"]',
            convert: x => parseRent(x, 1),
          },
        },
      },
    },
  },
};

exports.handler = async event => {
  // Log the event argument for debugging and for use in local development.
  // console.log(JSON.stringify(event, undefined, 2));
  const {httpMethod} = event;

  if (httpMethod === 'POST') {
    try {
      const scraped = await scrapeIt(availableUrl, scrapeOptions);
      const {data} = scraped;
      const {available} = data;

      const merged = available.reduce((acc, item) => {
        const {floorplan, prices, disclaimer} = item;
        if (floorplan) {
          //Type A
          return [...acc, item];
        } else if (prices.length > 0) {
          acc[acc.length - 1].prices = prices;
        }
        // NB: These is an empty record at the end we ignore
        return acc;
      }, []);

      const rents = merged.reduce((acc, item) => {
        const {beds, baths, floorplan, prices} = item;
        prices.forEach(unit => {
          acc.push({
            ...unit,
            beds,
            baths,
            floorplan,
          });
        });
        return acc;
      }, []);

      const result = await sendToInflux(rents);
      return {
        statusCode: 200,
        headers,
        body: JSON.stringify(rents),
      };
    } catch (err) {
      console.log('Caught err', err);
      return {
        statusCode: 500,
        headers,
        body: JSON.stringify(err),
      };
    }
  }

  return {
    statusCode: 204,
  };
};

function parseRent(rentString, index) {
  // $2,288-$2,922
  const parts = rentString.split('-');
  if (parts.length === 0) {
    console.log('No rent found', rentString);
    return 0;
  }
  if (parts.length === 1 && index > 0) {
    console.log('Non-range rent found', parts);
  }
  let part = parts[index] || parts[0]; // Fall back to 0th value
  part = part.replace('$', '').replace(',', '');
  part = parseInt(part, DECIMAL);
  return part;
}

async function sendToInflux(rents) {
  const points = rents
    .map(rent => {
      //{ UnitNumber: 512, min: 1568, max: 1701 }
      const {UnitNumber, min, max, sqft, floorplan, beds, baths} = rent;

      const pmin = new Point('rent')
        .tag('unitNumber', UnitNumber)
        .tag('sqft', sqft)
        .tag('floorplan', floorplan)
        .tag('beds', beds + '')
        .tag('baths', baths)
        .floatField('value', min); // Could be 'price'

      const pmax = new Point('rent')
        .tag('unitNumber', UnitNumber)
        .tag('sqft', sqft)
        .tag('floorplan', floorplan)
        .tag('beds', beds + '')
        .tag('baths', baths)
        .floatField('value', max);

      return [pmin, pmax];
    })
    .flat();

  if (NETLIFY_DEV) {
    points.forEach(point => console.log(point.toLineProtocol()));
  } else {
    await writeApi.writePoints(points);
    await writeApi.flush();
  }
  return points;
}
