module.exports = [
  {number: 301, sqft: 956, floorplan: 'E', beds: 1, baths: 1, views: 'W'},
  {number: 302, sqft: 1352, floorplan: 'F', beds: 2, baths: 2, views: 'NW'},
  {number: 303, sqft: 880, floorplan: 'G', beds: 1, baths: 1, views: 'SE'},
  {number: 304, sqft: 668, floorplan: 'H', beds: 1, baths: 1, views: 'SE'},
  {number: 305, sqft: 1328, floorplan: 'A', beds: 2, baths: 2, views: 'SW'},
  {number: 306, sqft: 815, floorplan: 'B', beds: 1, baths: 1, views: 'W'},
  {number: 307, sqft: 677, floorplan: 'C', beds: 1, baths: 1, views: 'W'},
  {number: 308, sqft: 890, floorplan: 'D', beds: 1, baths: 1, views: 'W'},
  {number: 401, sqft: 956, floorplan: 'E', beds: 1, baths: 1, views: 'W'},
  {number: 402, sqft: 838, floorplan: 'K', beds: 1, baths: 1, views: 'W'},
  {number: 403, sqft: 1407, floorplan: 'Z', beds: 2, baths: 2, views: 'NW'},
  {number: 404, sqft: 1021, floorplan: 'L', beds: 2, baths: 2, views: 'NE'},
  {number: 405, sqft: 789, floorplan: 'M', beds: 1, baths: 1, views: 'E'},
  {number: 406, sqft: 573, floorplan: 'N', beds: 0, baths: 1, views: 'E'},
  {number: 407, sqft: 889, floorplan: 'O', beds: 1, baths: 1, views: 'E'},
  {number: 408, sqft: 572, floorplan: 'N', beds: 0, baths: 1, views: 'E'},
  {number: 409, sqft: 789, floorplan: 'M', beds: 1, baths: 1, views: 'E'},
  {number: 410, sqft: 1021, floorplan: 'L', beds: 2, baths: 2, views: 'NE'},
  {number: 411, sqft: 1328, floorplan: 'A', beds: 2, baths: 2, views: 'SW'},
  {number: 412, sqft: 815, floorplan: 'B', beds: 1, baths: 1, views: 'W'},
  {number: 413, sqft: 677, floorplan: 'C', beds: 1, baths: 1, views: 'W'},
  {
    number: 414,
    sqft: 1128,
    floorplan: 'J',
    beds: 1,
    baths: 1,
    views: 'W',
    misc: '+ study',
  },
  {number: 501, sqft: 956, floorplan: 'E', beds: 1, baths: 1, views: 'W'},
  {number: 502, sqft: 838, floorplan: 'K', beds: 1, baths: 1, views: 'W'},
  {number: 503, sqft: 1407, floorplan: 'Z', beds: 2, baths: 2, views: 'NW'},
  {number: 504, sqft: 1021, floorplan: 'L', beds: 2, baths: 2, views: 'NE'},
  {number: 505, sqft: 789, floorplan: 'M', beds: 1, baths: 1, views: 'E'},
  {number: 506, sqft: 572, floorplan: 'N', beds: 0, baths: 1, views: 'E'},
  {number: 507, sqft: 889, floorplan: 'O', beds: 1, baths: 1, views: 'E'},
  {number: 508, sqft: 572, floorplan: 'N', beds: 0, baths: 1, views: 'E'},
  {number: 509, sqft: 789, floorplan: 'M', beds: 1, baths: 1, views: 'E'},
  {number: 510, sqft: 1021, floorplan: 'L', beds: 2, baths: 2, views: 'NE'},
  {number: 511, sqft: 1328, floorplan: 'A', beds: 2, baths: 2, views: 'SW'},
  {number: 512, sqft: 815, floorplan: 'B', beds: 1, baths: 1, views: 'W'},
  {number: 513, sqft: 677, floorplan: 'C', beds: 1, baths: 1, views: 'W'},
  {
    number: 514,
    sqft: 1128,
    floorplan: 'J',
    beds: 1,
    baths: 1,
    views: 'W',
    misc: '+ study',
  },
  {number: 601, sqft: 635, floorplan: 'Q', beds: 1, baths: 1, views: 'E'},
  {
    number: 602,
    sqft: 1281,
    floorplan: 'R',
    beds: 2,
    baths: 2,
    views: 'N',
    misc: '+ balcony',
  },
  {number: 603, sqft: 1021, floorplan: 'L', beds: 2, baths: 2, views: 'N'},
  {number: 604, sqft: 789, floorplan: 'M', beds: 1, baths: 1, views: 'E'},
  {number: 605, sqft: 572, floorplan: 'N', beds: 0, baths: 1, views: 'E'},
  {number: 606, sqft: 889, floorplan: 'O', beds: 1, baths: 1, views: 'E'},
  {number: 607, sqft: 572, floorplan: 'N', beds: 0, baths: 1, views: 'E'},
  {number: 608, sqft: 1131, floorplan: 'S', beds: 2, baths: 2, views: 'E'},
  {number: 609, sqft: 697, floorplan: 'T', beds: 1, baths: 1, views: 'E'},
  {number: 701, sqft: 956, floorplan: 'E', beds: 1, baths: 1, views: 'E'},
  {number: 702, sqft: 789, floorplan: 'M', beds: 1, baths: 1, views: 'E'},
  {number: 703, sqft: 822, floorplan: 'U', beds: 1, baths: 1, views: 'NW'},
  {number: 704, sqft: 689, floorplan: 'V', beds: 0, baths: 1, views: 'NW'},
  {number: 705, sqft: 569, floorplan: 'W', beds: 0, baths: 1, views: 'NW'},
  {number: 706, sqft: 789, floorplan: 'M', beds: 1, baths: 1, views: 'E'},
  {number: 707, sqft: 572, floorplan: 'N', beds: 0, baths: 1, views: 'E'},
  {number: 708, sqft: 889, floorplan: 'O', beds: 1, baths: 1, views: 'E'},
  {number: 709, sqft: 572, floorplan: 'N', beds: 0, baths: 1, views: 'E'},
  {number: 710, sqft: 789, floorplan: 'M', beds: 1, baths: 1, views: 'E'},
  {number: 711, sqft: 1021, floorplan: 'L', beds: 2, baths: 2, views: 'NE'},
  {number: 712, sqft: 1021, floorplan: 'L', beds: 2, baths: 2, views: 'NE'},
  {number: 713, sqft: 677, floorplan: 'C', beds: 1, baths: 1, views: 'W'},
  {number: 714, sqft: 677, floorplan: 'C', beds: 1, baths: 1, views: 'W'},
  {
    number: 715,
    sqft: 1128,
    floorplan: 'J',
    beds: 1,
    baths: 1,
    views: 'W',
    misc: '+ study',
  },
  {number: 801, sqft: 956, floorplan: 'E', beds: 1, baths: 1, views: 'W'},
  {number: 802, sqft: 789, floorplan: 'M', beds: 1, baths: 1, views: 'E'},
  {number: 803, sqft: 822, floorplan: 'U', beds: 1, baths: 1, views: 'NW'},
  {number: 804, sqft: 689, floorplan: 'V', beds: 0, baths: 1, views: 'N'},
  {number: 805, sqft: 569, floorplan: 'W', beds: 0, baths: 1, views: 'NW'},
  {number: 806, sqft: 789, floorplan: 'M', beds: 1, baths: 1, views: 'E'},
  {number: 807, sqft: 572, floorplan: 'N', beds: 0, baths: 1, views: 'E'},
  {number: 808, sqft: 889, floorplan: 'O', beds: 1, baths: 1, views: 'E'},
  {number: 809, sqft: 572, floorplan: 'N', beds: 0, baths: 1, views: 'E'},
  {number: 810, sqft: 789, floorplan: 'M', beds: 1, baths: 1, views: 'E'},
  {number: 811, sqft: 1021, floorplan: 'L', beds: 2, baths: 2, views: 'NE'},
  {number: 812, sqft: 1021, floorplan: 'L', beds: 2, baths: 2, views: 'NE'},
  {number: 813, sqft: 789, floorplan: 'M', beds: 1, baths: 1, views: 'E'},
  {number: 814, sqft: 677, floorplan: 'C', beds: 1, baths: 1, views: 'W'},
  {
    number: 815,
    sqft: 1128,
    floorplan: 'J',
    beds: 1,
    baths: 1,
    views: 'W',
    misc: '+ study',
  },
  {number: 901, sqft: 956, floorplan: 'E', beds: 1, baths: 1, views: 'W'},
  {number: 902, sqft: 789, floorplan: 'M', beds: 1, baths: 1, views: 'E'},
  {number: 903, sqft: 822, floorplan: 'U', beds: 1, baths: 1, views: 'NW'},
  {number: 904, sqft: 689, floorplan: 'V', beds: 0, baths: 1, views: 'N'},
  {number: 905, sqft: 569, floorplan: 'W', beds: 0, baths: 1, views: 'N'},
  {number: 906, sqft: 789, floorplan: 'M', beds: 1, baths: 1, views: 'E'},
  {number: 907, sqft: 572, floorplan: 'N', beds: 0, baths: 1, views: 'E'},
  {number: 908, sqft: 889, floorplan: 'O', beds: 1, baths: 1, views: 'E'},
  {number: 909, sqft: 572, floorplan: 'N', beds: 0, baths: 1, views: 'E'},
  {number: 910, sqft: 789, floorplan: 'M', beds: 1, baths: 1, views: 'E'},
  {number: 911, sqft: 1021, floorplan: 'L', beds: 2, baths: 2, views: 'NE'},
  {number: 912, sqft: 1021, floorplan: 'L', beds: 2, baths: 2, views: 'NE'},
  {number: 913, sqft: 789, floorplan: 'M', beds: 1, baths: 1, views: 'E'},
  {number: 914, sqft: 677, floorplan: 'C', beds: 1, baths: 1, views: 'W'},
  {
    number: 915,
    sqft: 1128,
    floorplan: 'J',
    beds: 1,
    baths: 1,
    views: 'W',
    misc: '+ study',
  },
  {number: 1001, sqft: 956, floorplan: 'E', beds: 1, baths: 1, views: 'W'},
  {number: 1003, sqft: 835, floorplan: 'X', beds: 1, baths: 1, views: 'NW'},
  {number: 1004, sqft: 1257, floorplan: 'Y', beds: 2, baths: 2, views: 'NW'},
  {number: 1005, sqft: 789, floorplan: 'M', beds: 1, baths: 1, views: 'E'},
  {number: 1006, sqft: 572, floorplan: 'N', beds: 0, baths: 1, views: 'E'},
  {number: 1007, sqft: 889, floorplan: 'O', beds: 1, baths: 1, views: 'E'},
  {number: 1009, sqft: 789, floorplan: 'M', beds: 1, baths: 1, views: 'E'},
  {number: 1010, sqft: 1021, floorplan: 'L', beds: 2, baths: 2, views: 'NE'},
  {number: 1011, sqft: 1021, floorplan: 'L', beds: 2, baths: 2, views: 'NE'},
  {number: 1012, sqft: 789, floorplan: 'M', beds: 1, baths: 1, views: 'E'},
  {number: 1013, sqft: 677, floorplan: 'C', beds: 1, baths: 1, views: 'W'},
  {number: 1014, sqft: 1021, floorplan: 'L', beds: 2, baths: 2, views: 'NE'},
  {number: 1101, sqft: 956, floorplan: 'E', beds: 1, baths: 1, views: 'W'},
  {number: 1103, sqft: 835, floorplan: 'X', beds: 1, baths: 1, views: 'NW'},
  {number: 1104, sqft: 1257, floorplan: 'Y', beds: 2, baths: 2, views: 'NW'},
  {number: 1105, sqft: 789, floorplan: 'M', beds: 1, baths: 1, views: 'E'},
  {number: 1106, sqft: 572, floorplan: 'N', beds: 0, baths: 1, views: 'E'},
  {number: 1107, sqft: 889, floorplan: 'O', beds: 1, baths: 1, views: 'E'},
  {number: 1108, sqft: 572, floorplan: 'N', beds: 0, baths: 1, views: 'E'},
  {number: 1109, sqft: 789, floorplan: 'M', beds: 1, baths: 1, views: 'E'},
  {number: 1110, sqft: 1021, floorplan: 'L', beds: 2, baths: 2, views: 'NE'},
  {number: 1111, sqft: 1021, floorplan: 'L', beds: 2, baths: 2, views: 'NE'},
  {number: 1112, sqft: 789, floorplan: 'M', beds: 1, baths: 1, views: 'E'},
  {number: 1113, sqft: 677, floorplan: 'C', beds: 1, baths: 1, views: 'W'},
  {
    number: 1114,
    sqft: 1128,
    floorplan: 'J',
    beds: 1,
    baths: 1,
    views: 'W',
    misc: '+ study',
  },
  {number: 1201, sqft: 956, floorplan: 'E', beds: 1, baths: 1, views: 'W'},
  {number: 1202, sqft: 789, floorplan: 'M', beds: 1, baths: 1, views: 'E'},
  {number: 1203, sqft: 835, floorplan: 'X', beds: 1, baths: 1, views: 'NW'},
  {number: 1204, sqft: 1257, floorplan: 'Y', beds: 2, baths: 2, views: 'NW'},
  {number: 1205, sqft: 789, floorplan: 'M', beds: 1, baths: 1, views: 'E'},
  {number: 1206, sqft: 572, floorplan: 'N', beds: 0, baths: 1, views: 'E'},
  {number: 1207, sqft: 889, floorplan: 'O', beds: 1, baths: 1, views: 'E'},
  {number: 1208, sqft: 572, floorplan: 'N', beds: 0, baths: 1, views: 'E'},
  {number: 1209, sqft: 789, floorplan: 'M', beds: 1, baths: 1, views: 'E'},
  {number: 1210, sqft: 1021, floorplan: 'L', beds: 2, baths: 2, views: 'NE'},
  {number: 1211, sqft: 1021, floorplan: 'L', beds: 2, baths: 2, views: 'NE'},
  {number: 1212, sqft: 789, floorplan: 'M', beds: 1, baths: 1, views: 'E'},
  {number: 1213, sqft: 677, floorplan: 'C', beds: 1, baths: 1, views: 'W'},
  {
    number: 1214,
    sqft: 1128,
    floorplan: 'J',
    beds: 1,
    baths: 1,
    views: 'W',
    misc: '+ study',
  },
  {number: 1401, sqft: 956, floorplan: 'E', beds: 1, baths: 1, views: 'W'},
  {number: 1402, sqft: 789, floorplan: 'M', beds: 1, baths: 1, views: 'E'},
  {number: 1403, sqft: 836, floorplan: 'X', beds: 1, baths: 1, views: 'NW'},
  {number: 1404, sqft: 1257, floorplan: 'Y', beds: 2, baths: 2, views: 'NW'},
  {number: 1405, sqft: 789, floorplan: 'M', beds: 1, baths: 1, views: 'E'},
  {number: 1406, sqft: 572, floorplan: 'N', beds: 0, baths: 1, views: 'E'},
  {number: 1407, sqft: 889, floorplan: 'O', beds: 1, baths: 1, views: 'E'},
  {number: 1408, sqft: 572, floorplan: 'N', beds: 0, baths: 1, views: 'E'},
  {number: 1409, sqft: 789, floorplan: 'M', beds: 1, baths: 1, views: 'E'},
  {number: 1410, sqft: 1021, floorplan: 'L', beds: 2, baths: 2, views: 'NE'},
  {number: 1411, sqft: 1021, floorplan: 'L', beds: 2, baths: 2, views: 'NE'},
  {
    number: 1414,
    sqft: 1128,
    floorplan: 'J',
    beds: 1,
    baths: 1,
    views: 'W',
    misc: '+ study',
  },
  {number: 1501, sqft: 956, floorplan: 'E', beds: 1, baths: 1, views: 'W'},
  {number: 1502, sqft: 789, floorplan: 'M', beds: 1, baths: 1, views: 'E'},
  {number: 1503, sqft: 836, floorplan: 'X', beds: 1, baths: 1, views: 'NW'},
  {number: 1505, sqft: 789, floorplan: 'M', beds: 1, baths: 1, views: 'E'},
  {number: 1506, sqft: 572, floorplan: 'N', beds: 0, baths: 1, views: 'E'},
  {number: 1507, sqft: 889, floorplan: 'O', beds: 1, baths: 1, views: 'E'},
  {number: 1508, sqft: 572, floorplan: 'N', beds: 0, baths: 1, views: 'E'},
  {number: 1509, sqft: 789, floorplan: 'M', beds: 1, baths: 1, views: 'E'},
  {number: 1510, sqft: 1021, floorplan: 'L', beds: 2, baths: 2, views: 'NE'},
  {number: 1511, sqft: 1021, floorplan: 'L', beds: 2, baths: 2, views: 'NE'},
  {number: 1512, sqft: 789, floorplan: 'M', beds: 1, baths: 1, views: 'E'},
  {number: 1513, sqft: 677, floorplan: 'C', beds: 1, baths: 1, views: 'W'},
  {
    number: 1514,
    sqft: 1128,
    floorplan: 'J',
    beds: 1,
    baths: 1,
    views: 'W',
    misc: '+ study',
  },
  {number: 1601, sqft: 956, floorplan: 'E', beds: 1, baths: 1, views: 'W'},
  {number: 1602, sqft: 789, floorplan: 'M', beds: 1, baths: 1, views: 'E'},
  {number: 1603, sqft: 836, floorplan: 'X', beds: 1, baths: 1, views: 'NW'},
  {number: 1604, sqft: 1257, floorplan: 'Y', beds: 2, baths: 2, views: 'NW'},
  {number: 1605, sqft: 789, floorplan: 'M', beds: 1, baths: 1, views: 'E'},
  {number: 1606, sqft: 572, floorplan: 'N', beds: 0, baths: 1, views: 'E'},
  {number: 1608, sqft: 572, floorplan: 'N', beds: 0, baths: 1, views: 'E'},
  {number: 1609, sqft: 789, floorplan: 'M', beds: 1, baths: 1, views: 'E'},
  {number: 1610, sqft: 1021, floorplan: 'L', beds: 2, baths: 2, views: 'NE'},
  {number: 1612, sqft: 789, floorplan: 'M', beds: 1, baths: 1, views: 'E'},
  {number: 1613, sqft: 677, floorplan: 'C', beds: 1, baths: 1, views: 'W'},
  {
    number: 1614,
    sqft: 1128,
    floorplan: 'J',
    beds: 1,
    baths: 1,
    views: 'W',
    misc: '+ study',
  },
  {number: 1702, sqft: 789, floorplan: 'M', beds: 1, baths: 1, views: 'E'},
  {number: 1703, sqft: 836, floorplan: 'X', beds: 1, baths: 1, views: 'NW'},
  {number: 1704, sqft: 1257, floorplan: 'Y', beds: 2, baths: 2, views: 'NW'},
  {number: 1705, sqft: 789, floorplan: 'M', beds: 1, baths: 1, views: 'E'},
  {number: 1706, sqft: 572, floorplan: 'N', beds: 0, baths: 1, views: 'E'},
  {number: 1707, sqft: 889, floorplan: 'O', beds: 1, baths: 1, views: 'E'},
  {number: 1708, sqft: 572, floorplan: 'N', beds: 0, baths: 1, views: 'E'},
  {number: 1709, sqft: 789, floorplan: 'M', beds: 1, baths: 1, views: 'E'},
  {number: 1710, sqft: 1021, floorplan: 'L', beds: 2, baths: 2, views: 'NE'},
  {number: 1711, sqft: 1021, floorplan: 'L', beds: 2, baths: 2, views: 'NE'},
  {number: 1712, sqft: 789, floorplan: 'M', beds: 1, baths: 1, views: 'E'},
  {number: 1713, sqft: 677, floorplan: 'C', beds: 1, baths: 1, views: 'W'},
  {
    number: 1714,
    sqft: 1128,
    floorplan: 'J',
    beds: 1,
    baths: 1,
    views: 'W',
    misc: '+ study',
  },
  {number: 1801, sqft: 956, floorplan: 'E', beds: 1, baths: 1, views: 'W'},
  {number: 1802, sqft: 789, floorplan: 'M', beds: 1, baths: 1, views: 'E'},
  {number: 1803, sqft: 836, floorplan: 'X', beds: 1, baths: 1, views: 'NW'},
  {number: 1804, sqft: 1257, floorplan: 'Y', beds: 2, baths: 2, views: 'NW'},
  {number: 1805, sqft: 789, floorplan: 'M', beds: 1, baths: 1, views: 'E'},
  {number: 1806, sqft: 572, floorplan: 'N', beds: 0, baths: 1, views: 'E'},
  {number: 1807, sqft: 889, floorplan: 'O', beds: 1, baths: 1, views: 'E'},
  {number: 1808, sqft: 572, floorplan: 'N', beds: 0, baths: 1, views: 'E'},
  {number: 1809, sqft: 789, floorplan: 'M', beds: 1, baths: 1, views: 'E'},
  {number: 1810, sqft: 1021, floorplan: 'L', beds: 2, baths: 2, views: 'NE'},
  {number: 1811, sqft: 1021, floorplan: 'L', beds: 2, baths: 2, views: 'NE'},
  {number: 1812, sqft: 789, floorplan: 'M', beds: 1, baths: 1, views: 'E'},
  {number: 1813, sqft: 677, floorplan: 'C', beds: 1, baths: 1, views: 'W'},
  {
    number: 1814,
    sqft: 1128,
    floorplan: 'J',
    beds: 1,
    baths: 1,
    views: 'W',
    misc: '+ study',
  },
];
