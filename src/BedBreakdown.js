import React from 'react';

import Card from 'react-bootstrap/Card';

import {
  Resizable,
} from 'react-timeseries-charts';

import {BarChart, Bar, XAxis, YAxis, Legend} from 'recharts';
import {schemeCategory20} from 'd3-scale';

function BedBreakdown(props) {
  const {today} = props
  const total = Object.values(today).reduce((sum, val) => sum + val, 0)
  const height = 100;

  return (
    <>
      <Card.Title>Breakdown by bedrooms</Card.Title>
      <Resizable>
        <BarChart layout="vertical" height={height} data={[today]}>
          {today.zero > 0 ? <Bar
            isAnimationActive={false}
            dataKey="zero"
            stackId="beds"
            label={{fill: schemeCategory20[0], fontSize: 20, dataKey: 'zero'}}
            fill={schemeCategory20[1]}
            name="Studio"
          /> : null }
          {today.one > 0 ? <Bar
            isAnimationActive={false}
            dataKey="one"
            stackId="beds"
            label={{fill: schemeCategory20[2], fontSize: 20, dataKey: 'one'}}
            fill={schemeCategory20[3]}
            name="1 Bedroom"
          /> : null }
          {today.two > 0 ? <Bar
            isAnimationActive={false}
            dataKey="two"
            stackId="beds"
            label={{fill: schemeCategory20[4], fontSize: 20, dataKey: 'two'}}
            fill={schemeCategory20[5]}
            name="2 Bedroom"
          /> : null }
          <XAxis type="number" domain={[0, 'dataMax']} />
          <YAxis dataKey="date" type="category" tickFormatter={() => total} />
          <Legend verticalAlign="top" />
        </BarChart>
      </Resizable>
    </>
  );
}

export default BedBreakdown;
