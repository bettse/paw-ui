import initialState from './initialState';
import {SELECT_UNIT, FILTER_FLOORPLAN} from '../actions/actionTypes';

export default function unit(state = initialState, action) {
  switch (action.type) {
    case SELECT_UNIT:
      const {selected} = action;
      return {
        ...state,
        selected,
      };
    case FILTER_FLOORPLAN:
      const {floorplan} = action;
      return {
        ...state,
        filter: {...state.filter, floorplan},
      };
    default:
      return state;
  }
}
