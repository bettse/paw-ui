import React from 'react';

import Navbar from 'react-bootstrap/Navbar'
import Badge from 'react-bootstrap/Badge'
import Nav from 'react-bootstrap/Nav'

import {When} from 'react-if';

function millisecondsToStr (milliseconds) {
	function numberEnding (number) {
		return (number > 1) ? 's' : '';
	}

	var temp = Math.floor(milliseconds / 1000);
	const years = Math.floor(temp / 31536000);
	if (years) {
		return years + ' year' + numberEnding(years);
	}

	const days = Math.floor((temp %= 31536000) / 86400);
	if (days) {
		return days + ' day' + numberEnding(days);
	}
	const hours = Math.floor((temp %= 86400) / 3600);
	if (hours) {
		return hours + ' hour' + numberEnding(hours);
	}
	const minutes = Math.floor((temp %= 3600) / 60);
	if (minutes) {
		return minutes + ' minute' + numberEnding(minutes);
	}
	const seconds = temp % 60;
	if (seconds) {
		return seconds + ' second' + numberEnding(seconds);
	}
	return 'less than a second';
}

function Header(props) {
  const { error, audit } = props;

  const auditDetails = () => {
    const { count, last } = audit;
    const now = new Date().getTime();
    const ago = (now - last)
    return (
      <Nav.Item>
        Last run ({count} units): &nbsp;
        <Badge variant="primary">{millisecondsToStr(ago)} ago</Badge>
      </Nav.Item>
    )
  }

  return (
    <Navbar>
      <Navbar.Brand>
        <a href="/">PAW Prices</a>
      </Navbar.Brand>
      <Nav>
        {audit && auditDetails()}
        <When condition={error !== null}>
          {error}
        </When>
      </Nav>
    </Navbar>
  );
}

export default Header;
