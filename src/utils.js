import {format} from 'd3-format';
import {timeParse, timeFormat} from 'd3-time-format';
import {InfluxDB, typeSerializers} from '@influxdata/influxdb-client';

export const rentFormat = '$,.0f';
export const rentFormatter = rent => (rent ? format(rentFormat)(rent) : '--');

export const sqftFormat = format(',.0f');
export const moneyFormat = format('$,.2f');

export const formatDate = timeFormat('%b %d');
export const parseDate = timeParse('%Y-%m-%d');

const {
  REACT_APP_INFLUXDB_V2_URL,
  REACT_APP_INFLUXDB_V2_ORG,
  REACT_APP_INFLUXDB_V2_TOKEN,
} = process.env;

typeSerializers['dateTime:RFC3339'] = (dt) => (new Date(dt)).getTime()

export const influxApi = new InfluxDB({url: REACT_APP_INFLUXDB_V2_URL, token: REACT_APP_INFLUXDB_V2_TOKEN}).getQueryApi(REACT_APP_INFLUXDB_V2_ORG);
