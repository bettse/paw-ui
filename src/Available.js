import React, {useEffect, useState} from 'react';

import Card from 'react-bootstrap/Card';
import Badge from 'react-bootstrap/Badge';
import Image from 'react-bootstrap/Image';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Tooltip from 'react-bootstrap/Tooltip';

import BootstrapTable from 'react-bootstrap-table-next';
import filterFactory, {selectFilter} from 'react-bootstrap-table2-filter';

import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import 'react-bootstrap-table2-filter/dist/react-bootstrap-table2-filter.min.css';

import useWindowSize from './useWindowSize';

import {rentFormatter, sqftFormat} from './utils';

function uniq(value, index, self) {
  return self.indexOf(value) === index;
}

const unitFloor = unit => Math.floor(unit.unitNumber / 100);

const bedDescs = ['Studio', '1 Bed', '2 Bed'];

const selectStyle = {
  height: 16,
  padding: 0,
  textAlign: 'center',
  fontSize: 10,
  overflow: 'hidden',
  margin: 0,
  borderRadius: '.5em',
  MozAppearance: 'none',
  WebkitAppearance: 'none',
  appearance: 'none',
};

const floorplanUnitNumber = (unitNumber, unit) => {
  const {price} = unit;
  if (price === undefined) {
    return <span> {unitNumber} </span>;
  }
  const {today, yesterday} = price;
  if (today === undefined) {
    return (
      <span>
        <del>{unitNumber}</del>
      </span>
    );
  }
  if (yesterday === undefined) {
    return (
      <span>
        <strong>{unitNumber}</strong>
      </span>
    );
  }
  return <span> {unitNumber} </span>;
};

const bedsFormat = (beds, unit) => {
  const {study} = unit;
  const bedDesc = bedDescs[beds];
  return study ? `${bedDesc} + study` : bedDesc;
};

const rentStyle = value => {
  if (value > 0) {
    return 'text-danger';
  } else if (value < 0) {
    return 'text-success';
  } else {
    return '';
  }
};

const rentDisplay = (price, unit) => {
  if (!price) {
    return '-';
  }
  const {unitNumber} = unit;
  const {today, delta} = price;
  if (!today) {
    return '-';
  }

  const {min, max} = today;
  const {min: dMin, max: dMax} = delta || {min: 0, max: 0};

  const deltaTooltip = value => (
    <Tooltip id={`tooltip-${unitNumber}-${value}`}>
      <div>{rentFormatter(value)}</div>
    </Tooltip>
  );

  let minMarkup, maxMarkup;
  if (dMin === 0) {
    minMarkup = <span className={rentStyle(dMin)}>{rentFormatter(min)}</span>;
  } else {
    minMarkup = (
      <OverlayTrigger placement="left" overlay={deltaTooltip(dMin)}>
        <span className={rentStyle(dMin)}>{rentFormatter(min)}</span>
      </OverlayTrigger>
    );
  }
  if (dMax === 0) {
    maxMarkup = <span className={rentStyle(dMax)}>{rentFormatter(max)}</span>;
  } else {
    maxMarkup = (
      <OverlayTrigger placement="right" overlay={deltaTooltip(dMax)}>
        <span className={rentStyle(dMax)}>{rentFormatter(max)}</span>
      </OverlayTrigger>
    );
  }

  return (
    <div>
      {minMarkup}-{maxMarkup}
    </div>
  );
};

const floorplanFormat = (floorplan, unit) => {
  const {beds} = unit;
  // Pretty close to the d3 color palette I am using
  const styleMap = ['info', 'warning', 'success'];
  return (
    <Badge variant={styleMap[beds || 0]} size="sm">
      {unit.floorplan}
    </Badge>
  );
};

const expandComponent = unit => {
  const {floorplan} = unit;
  return (
    <Image
      fluid
      src={`${
        process.env.PUBLIC_URL
      }/floorplans/park_ave_unit_${floorplan.toLowerCase()}-768x768-q75.png`}
      alt={`Top down view of floorplan ${floorplan}`}
    />
  );
};

const streakSortFunc = (a, b, order) => {
  if (a === '-') {
    a = 0;
  }
  if (b === '-') {
    b = 0;
  }
  if (order === 'asc') {
    return b - a;
  } else {
    return a - b;
  }
};

const rentSortFunc = (a, b, order) => {
  if (a.today && b.today) {
    if (order === 'asc') {
      return b.today.max - a.today.max;
    } else {
      return a.today.min - b.today.min;
    }
  } else if (a.today && !b.today) {
    if (order === 'asc') {
      return 1;
    } else {
      return -1;
    }
  } else if (!a.today && b.today) {
    if (order === 'asc') {
      return -1;
    } else {
      return 1;
    }
  }
  return 0;
};

const bedroomOptions = units => {
  return bedDescs.reduce((options, desc, beds) => {
    const count = units.filter(d => d.beds === beds).length;
    options[beds] = `${desc}: ${count}`;
    return options;
  }, {});
};

const floorOptions = units => {
  const floors = units.map(({floor}) => floor).filter(uniq);
  floors.sort((a, b) => a - b);
  return floors.reduce((options, floor) => {
    const count = units.filter(d => d.floor === floor).length;
    options[floor] = `${floor}: ${count}`;
    return options;
  }, {});
};

const floorplanOptions = units => {
  const base = 36;
  const skip = 10;
  const options = {};
  for (var i = 0; i < 26; i++) {
    const letter = (i + skip).toString(base).toUpperCase();
    const count = units.filter(d => d.floorplan === letter).length;

    if (count > 0) {
      options[letter] = `${letter}: ${count}`;
    }
  }
  return options;
};

const selectClasses = unit =>
  ['bg-info', 'bg-warning', 'bg-success'][unit.beds || 0];

const expandRow = {
  renderer: expandComponent,
  showExpandColumn: false,
};

let localFloorplanFilter = console.log;

function Available(props) {
  const {unitActions, units, streaks, selected, filter} = props;
  const {loadDetail} = unitActions;
  const [hiddenColumns, setHiddenColumns] = useState([]);
  const windowSize = useWindowSize();

  units.forEach(unit => {
    unit.floor = unitFloor(unit);
  });

  if (streaks) {
    units.forEach(unit => {
      const {unitNumber} = unit;
      unit.streak = streaks[unitNumber] || '-';
    });
  }

  useEffect(() => {
    // If selectFitler has exported the function for filtering the floorplan, call it
    if (localFloorplanFilter) {
      localFloorplanFilter(filter.floorplan);
    }
  }, [filter.floorplan]);

  useEffect(() => {
    // hiddenColumns is based on the dataField value
    switch (windowSize) {
      case 'xs':
        setHiddenColumns(['streak', 'beds']);
        break;
      case 'sm':
        setHiddenColumns(['streak']);
        break;
      case 'md':
      case 'lg':
      case 'xl':
      default:
        setHiddenColumns([]);
    }
  }, [windowSize]);

  const selectRowOptions = {
    mode: 'radio',
    clickToSelect: true,
    clickToExpand: true,
    hideSelectColumn: true,
    classes: selectClasses,
    onSelect: loadDetail,
    selected: [],
  };

  if (selected) {
    selectRowOptions.selected = [selected.unitNumber];
  }

  const columns = [
    {
      dataField: 'unitNumber',
      sort: true,
      formatter: floorplanUnitNumber,
      text: 'Unit',
      filter: selectFilter({
        placeholder: '*',
        style: selectStyle,
        options: floorOptions(units),
        onFilter: (floor, units) => units.filter(u => u.floor + '' === floor),
      }),
    },
    {
      dataField: 'floorplan',
      filter: selectFilter({
        placeholder: '*',
        style: selectStyle,
        options: floorplanOptions(units),
        getFilter: filter => (localFloorplanFilter = filter),
      }),
      align: 'center',
      formatter: floorplanFormat,
      classes: 'text-uppercase',
      text: 'Floor plan',
    },
    {
      dataField: 'sqft',
      sort: true,
      formatter: sqftFormat,
      text: 'Sq. Ft.',
    },
    {
      dataField: 'beds',
      filter: selectFilter({
        placeholder: '*',
        style: selectStyle,
        options: bedroomOptions(units),
      }),
      formatter: bedsFormat,
      text: 'Beds',
    },
    {
      dataField: 'price',
      sort: true,
      sortFunc: rentSortFunc,
      formatter: rentDisplay,
      text: 'Rent',
      headerStyle: {
        width: '13ch',
      },
    },
    {
      dataField: 'streak',
      sort: true,
      text: 'Streak',
      sortFunc: streakSortFunc,
    },
  ];

  return (
    <Card>
      <Card.Header>Available units</Card.Header>
      <BootstrapTable
        keyField="unitNumber"
        data={units}
        columns={columns.filter(
          ({dataField}) => !hiddenColumns.includes(dataField),
        )}
        selectRow={selectRowOptions}
        expandRow={expandRow}
        filter={filterFactory()}
        defaultSorted={[{dataField: 'unitNumber', order: 'asc'}]}
        noDataIndication="No units to display"
        bootstrap4
        condensed
      />
    </Card>
  );
}

export default Available;
