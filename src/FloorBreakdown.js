import React from 'react';

import Card from 'react-bootstrap/Card';

import { Resizable } from 'react-timeseries-charts';
import {BarChart, Bar, XAxis, YAxis, Legend} from 'recharts';
import {schemeCategory20b} from 'd3-scale';

function FloorBreakdown(props) {
  const { breakdown } = props

  const total = Object.values(breakdown).reduce((sum, val) => sum + val, 0)
  const keys = Object.keys(breakdown)
  const height = 100;

  return (
    <>
      <Card.Title>Breakdown by floor</Card.Title>
      <Resizable>
        <BarChart layout="vertical" height={height} data={[breakdown]}>
          {keys.map((letter, index) => {
            const fill = schemeCategory20b[index * 2 + 1];
            const label = schemeCategory20b[index * 2 + 0];
            return (
              <Bar
                key={letter}
                isAnimationActive={false}
                dataKey={letter}
                stackId="floor"
                label={{fill: label, fontSize: 20, dataKey: letter}}
                fill={fill}
              />
            );
          })}
          <XAxis type="number" domain={[0, 'dataMax']} />
          <YAxis dataKey="date" type="category" tickFormatter={() => total} />
          <Legend
            verticalAlign="top"
            iconType="circle"
          />
        </BarChart>
      </Resizable>
    </>
  );
}

export default FloorBreakdown;
