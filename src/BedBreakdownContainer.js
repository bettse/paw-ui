import React, { useState, useEffect } from 'react';
import {connect} from 'react-redux';
import {flux} from '@influxdata/influxdb-client';

import useLocalStorage from './useLocalStorage';
import {influxApi} from './utils';
import BedBreakdown from './BedBreakdown';

const {
  REACT_APP_INFLUXDB_V2_BUCKET,
} = process.env;

const BedBreakdownContainer = () => {
  const [error, setError] = useState(null);
  const [today, setToday] = useLocalStorage('bedbreakdown-today', null);

  useEffect(() => {
    async function fetchToday() {
      try {
        const query = flux`
          from(bucket: "${REACT_APP_INFLUXDB_V2_BUCKET}")
          |> range(start: -1d)
          |> filter(fn: (r) => r["_measurement"] == "rent")
          |> filter(fn: (r) => r["_field"] == "value")
          |> filter(fn: (r) => r["complex"] == "parkavewest")
          |> filter(fn: (r) => r["scraper"] == "netlify")
          |> drop(columns: ["study"])
          |> unique(column: "unitNumber")
          |> aggregateWindow(every: inf, fn: last, createEmpty: false)
          |> map(fn: (r) => ({ r with beds: if exists r.beds then r.beds else "0" }))
          |> group(columns: ["_time", "complex"])
          |> reduce(
            fn: (r, accumulator) => ({
              // r with
              zero: accumulator.zero + (if r.beds == "0" then 1 else 0),
              one: accumulator.one + (if r.beds == "1" then 1 else 0),
              two: accumulator.two + (if r.beds == "2" then 1 else 0)
            }),
            identity: {zero: 0, one: 0, two: 0}
          )
        `;

        const results = await influxApi.collectRows(query)
        // console.log({results})
        const [result] = results
        const {zero, one, two} = result;

        setToday({zero, one, two});
      } catch (err) {
        console.error('fetch today err', err);
        setError(err)
      }
    }

    fetchToday();
  }, []);

  if (error) return <div>Error loading history</div>;
  if (!today) { return null; }

  return <BedBreakdown today={today} />;
};

function mapStateToProps(state, props) {
  return {
    ...state.unitReducer,
    ...props,
  };
}

function mapDispatchToProps(dispatch) {
  return {};
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(BedBreakdownContainer);
