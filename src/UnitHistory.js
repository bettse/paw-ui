import React, {useState, useEffect} from 'react';

import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';
import {TimeRange} from 'pondjs';

import {
  Baseline,
  Charts,
  ChartContainer,
  ChartRow,
  YAxis,
  LineChart,
  styler,
  Resizable,
} from 'react-timeseries-charts';

import {rentFormat, rentFormatter} from './utils';

import './UnitHistory.css';

const height = 250;
const style = styler([
  {key: 'min', color: 'steelblue', width: 2},
  {key: 'max', color: 'red', width: 2},
]);
const fortnight = 2 * 7 * 24 * 60 * 60 * 1000;

const trackerStyle = {
  box: {
    box: { display: 'none', },
    label: {
      fontSize: '24px',
    },
  }
}

function UnitHistory(props) {
  const {number, series, streak, highest, lowest} = props;

  const [selection, setSelection] = useState(null);
  const [tracker, setTracker] = useState(undefined);
  const [timerange, setTimerange] = useState(undefined);

  useEffect(() =>  {
    setSelection(null);
    setTracker(undefined);
    setTimerange(undefined);
  }, [number])

  let minValue, maxValue;
  if (tracker) {
    const index = series.bisect(tracker);
    const trackerEvent = series.at(index);
    minValue = rentFormatter(trackerEvent.get('min'));
    maxValue = rentFormatter(trackerEvent.get('max'));
  }

  let initialTimerange = series.range()
  if (series.size() === 1) {
    initialTimerange = TimeRange.lastDay();
  }

  const trackerInfoValues = [];
  if (!selection || selection === 'max') {
    trackerInfoValues.push({label: 'Max', value: maxValue})
  }
  if (!selection || selection === 'min') {
    trackerInfoValues.push({label: 'Min', value: minValue})
  }

  const today = series.atLast().toJSON()
  const baselines = [];
  if (today.data.min !== lowest) {
    baselines.push(<Baseline key="lowest" axis="y" value={lowest} label="Lowest" position="right"/>)
  }
  if (today.data.max !== highest) {
    baselines.push(<Baseline key="highest" axis="y" value={highest} label="Highest" position="right"/>)
  }

  return (
    <>
      <Card.Title>{`${series.range().humanizeDuration()} rent history for ${number}`}</Card.Title>
      <ListGroup horizontal className="mb-2">
        <ListGroup.Item>Current streak: {streak} days</ListGroup.Item>
        <ListGroup.Item>Lowest: {rentFormatter(lowest)}</ListGroup.Item>
        <ListGroup.Item>Highest: {rentFormatter(highest)}</ListGroup.Item>
      </ListGroup>
      <Resizable>
        <ChartContainer
          timeRange={timerange || initialTimerange}
          maxTime={series.range().end()}
          minTime={series.range().begin()}
          trackerPosition={tracker}
          trackerStyle={trackerStyle}
          onTrackerChanged={t => setTracker(t)}
          onBackgroundClick={() => setSelection(null)}
          onTimeRangeChanged={tr => setTimerange(tr)}
          minDuration={fortnight}
          timeAxisAngledLabels={true}
          enablePanZoom={series.size() > 1}>
          <ChartRow
            height={height}
            trackerShowTime={false}
            trackerInfoValues={trackerInfoValues}
            trackerInfoHeight={10 + trackerInfoValues.length * 16}>
            <YAxis
              id="y"
              max={Math.max(series.max('max'), highest)}
              min={0}
              type="linear"
              format={rentFormat}
            />
            <Charts>
              <LineChart
                axis="y"
                series={series}
                columns={['min', 'max']}
                style={style}
                interpolation="curveStepAfter"
                selection={selection}
                onSelectionChange={s => setSelection(s)}
              />
              {baselines}
            </Charts>
          </ChartRow>
        </ChartContainer>
      </Resizable>
    </>
  );
}

export default UnitHistory;
