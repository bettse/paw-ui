import React, {useState, useEffect} from 'react';
import {connect} from 'react-redux';
import {flux} from '@influxdata/influxdb-client';
import {TimeSeries, TimeEvent} from 'pondjs';

import useLocalStorage from './useLocalStorage';
import {influxApi} from './utils';
import AvgHistory from './AvgHistory';

const {REACT_APP_INFLUXDB_V2_BUCKET} = process.env;

const AvgHistoryContainer = props => {
  const {groupBy = 'complex'} = props;
  const [error, setError] = useState(null);
  const [rawSeries, setRawSeries] = useLocalStorage(
    `AvgHistory-${groupBy}-rawSeries`,
    null,
  );
  const [series, setSeries] = useState(null);

  useEffect(() => {
    async function fetchRawSeries() {
      try {
        const query = flux`
          from(bucket: "${REACT_APP_INFLUXDB_V2_BUCKET}")
          |> range(start: -1y)
          |> filter(fn: (r) => r["_measurement"] == "rent")
          |> filter(fn: (r) => r["_field"] == "value")
          |> map(fn: (r) => ({ r with beds: if exists r.beds then r.beds else "0" }))
          |> drop(columns: ["sqft", "baths", "floorplan", "study", "balcony"])
          |> group(columns: ["${groupBy}"])
          |> aggregateWindow(every: 24h, fn: mean, createEmpty: false)
          |> yield(name: "${groupBy}")
        `;

        const results = await influxApi.collectRows(query);
        // console.log({results})

        setRawSeries(results);
      } catch (err) {
        console.error('fetch data err', err);
        setError(err);
      }
    }
    fetchRawSeries();
  }, []);

  useEffect(() => {
    if (rawSeries === null) {
      return;
    }

    // rawSeries have rows for zero beds, followed by rows for one beds, ..., so we corrolate them based on time
    const joined = rawSeries.reduce((acc, val) => {
      const {_time, _value, result} = val;
      const key = val[result];
      acc[_time] = Object.assign(acc[_time] || {}, {_time, [key]: _value});
      return acc;
    }, {});

    const events = Object.values(joined).map(record => {
      const {_time, ...rest} = record;
      return new TimeEvent(_time, rest);
    });
    events.sort((a, b) => a.key() - b.key());

    const series = new TimeSeries({
      name: 'avgHistory',
      events,
    });
    // console.log(series.toJSON())

    setSeries(series);
  }, [rawSeries]);

  if (error) console.log(error);
  if (!series) {
    return null;
  }

  return <AvgHistory series={series} />;
};

function mapStateToProps(state, props) {
  return {
    ...state.unitReducer,
    ...props,
  };
}

function mapDispatchToProps(dispatch) {
  return {};
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AvgHistoryContainer);
