import React, {useState, useEffect} from 'react';
import {connect} from 'react-redux';
import {flux} from '@influxdata/influxdb-client';
import {TimeSeries, TimeEvent} from 'pondjs';

import useLocalStorage from './useLocalStorage';
import {influxApi} from './utils';
import BedBreakdownHistory from './BedBreakdownHistory';

const {REACT_APP_INFLUXDB_V2_BUCKET} = process.env;

const BedBreakdownHistoryContainer = () => {
  const [error, setError] = useState(null);
  const [rawHistory, setRawHistory] = useLocalStorage(
    `bedbreakdownhistory-rawHistory`,
    null,
  );
  const [history, setHistory] = useState(null);

  useEffect(() => {
    async function fetchHistory() {
      try {
        const query = flux`
          from(bucket: "${REACT_APP_INFLUXDB_V2_BUCKET}")
          |> range(start: -1y)
          |> filter(fn: (r) => r["_measurement"] == "rent")
          |> filter(fn: (r) => r["_field"] == "value")
          |> filter(fn: (r) => r["complex"] == "parkavewest")
          |> drop(columns: ["study", "scraper"])
          |> aggregateWindow(every: 24h, fn: last, createEmpty: false)
          |> map(fn: (r) => ({ r with beds: if exists r.beds then r.beds else "0" }))
          |> group(columns: ["_time", "complex"])
          |> reduce(
            fn: (r, accumulator) => ({
              // r with
              zero: accumulator.zero + (if r.beds == "0" then 1 else 0),
              one: accumulator.one + (if r.beds == "1" then 1 else 0),
              two: accumulator.two + (if r.beds == "2" then 1 else 0)
            }),
            identity: {zero: 0, one: 0, two: 0}
          )
        `;

        const results = await influxApi.collectRows(query);
        // console.log({results})

        setRawHistory(results);
      } catch (err) {
        console.error('fetch history err', err);
        setError(err);
      }
    }
    fetchHistory();
  }, []);

  useEffect(() => {
    if (rawHistory === null || rawHistory.length === 0) {
      return;
    }
    const events = rawHistory.map(record => {
      const {_time, zero, one, two} = record;
      return new TimeEvent(_time, {zero, one, two});
    });

    const series = new TimeSeries({
      name: 'byBed',
      events,
    });
    // console.log(series.toJSON())

    setHistory(series);
  }, [rawHistory]);

  if (error) console.log(error);
  if (!history) {
    return null;
  }

  return <BedBreakdownHistory series={history} />;
};

function mapStateToProps(state, props) {
  return {
    ...state.unitReducer,
    ...props,
  };
}

function mapDispatchToProps(dispatch) {
  return {};
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(BedBreakdownHistoryContainer);
