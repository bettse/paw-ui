import * as types from './actionTypes';

export function loadDetail(unit) {
  return (dispatch, getState) => {
    return dispatch({type: types.SELECT_UNIT, selected: unit});
  };
}

export function filterFloorplan(floorplan) {
  return (dispatch, getState) => {
    return dispatch({type: types.FILTER_FLOORPLAN, floorplan});
  };
}
