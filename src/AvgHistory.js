import React, {useState} from 'react';

import Card from 'react-bootstrap/Card';

import {
  Charts,
  ChartContainer,
  ChartRow,
  YAxis,
  LineChart,
  styler,
  Resizable,
} from 'react-timeseries-charts';
import {schemeCategory20} from 'd3-scale';

import {rentFormat, rentFormatter} from './utils';

import './AvgHistory.css';

const height = 250;
const style = styler([
  {key: 'parkavewest', color: 'gold', width: 1},
  {key: '0', color: schemeCategory20[0], selected: schemeCategory20[1]},
  {key: '1', color: schemeCategory20[2], selected: schemeCategory20[3]},
  {key: '2', color: schemeCategory20[4], selected: schemeCategory20[5]},
]);

const trackerStyle = {
  box: {
    box: { display: 'none', },
    label: {
      fontSize: '24px',
    },
  }
}
const fortnight = 2 * 7 * 24 * 60 * 60 * 1000;

const columnDesc = {
  'parkavewest': 'Avg',
  '0': 'Studio',
  '1': '1 Bedroom',
  '2': '2 Bedroom',
}

function AvgHistory(props) {
  const {series} = props;

  const [tracker, setTracker] = useState(undefined);
  const [timerange, setTimerange] = useState(undefined);

  const columns = series.columns();
  const max = Math.max(...columns.map(c => series.max(c)))
  const min = Math.min(...columns.map(c => series.min(c)))

	const scrub = {}
  if (tracker) {
    const index = series.bisect(tracker);
    const trackerEvent = series.at(index);
    columns.forEach(column => {
      scrub[column] = rentFormatter(trackerEvent.get(column));
    })
  }
  const trackerInfoValues = columns.map(column => {
    return {label: columnDesc[column] || column, value: scrub[column]}
  })

  return (
    <>
      <Card.Title>Avg rent</Card.Title>
      <Resizable>
        <ChartContainer
          timeRange={timerange || series.range()}
          maxTime={series.range().end()}
          minTime={series.range().begin()}
          trackerPosition={tracker}
          trackerStyle={trackerStyle}
          onTrackerChanged={t => setTracker(t)}
          onTimeRangeChanged={tr => setTimerange(tr)}
          minDuration={fortnight}
          timeAxisAngledLabels={true}
          enablePanZoom={true}>
          <ChartRow
            height={height}
            trackerShowTime={false}
            trackerInfoValues={trackerInfoValues}
            trackerInfoHeight={10 + trackerInfoValues.length * 16}>
            <YAxis
              id="y"
              max={max}
              min={min}
              type="linear"
              format={rentFormat}
            />
            <Charts>
              <LineChart
                axis="y"
                series={series}
                columns={series.columns()}
                style={style}
                interpolation="curveStepAfter"
              />
            </Charts>
          </ChartRow>
        </ChartContainer>
      </Resizable>
    </>
  );
}

export default AvgHistory;
