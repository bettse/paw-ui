import React, { useState, useEffect } from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {flux} from '@influxdata/influxdb-client';

import useLocalStorage from './useLocalStorage';
import * as unitActions from './actions/unitActions';
import {influxApi} from './utils';
import Header from './Header';

const {
  REACT_APP_INFLUXDB_V2_BUCKET,
} = process.env;

const HeaderContainer = props => {
  const [error, setError] = useState(null);
  const [audit, setAudit] = useLocalStorage('header-audit', null);

  useEffect(() => {
    async function fetchData() {
      try {
        const query = flux`
          from(bucket: "${REACT_APP_INFLUXDB_V2_BUCKET}")
            |> range(start: -24h)
            |> filter(fn: (r) => r["_measurement"] == "rent")
            |> filter(fn: (r) => r["_field"] == "value")
            |> filter(fn: (r) => r["complex"] == "parkavewest")
            |> group(columns: ["complex"])
            |> unique(column: "unitNumber")
            |> count()
            |> yield(name: "count")

          from(bucket: "${REACT_APP_INFLUXDB_V2_BUCKET}")
            |> range(start: -7d)
            |> filter(fn: (r) => r["_measurement"] == "rent")
            |> filter(fn: (r) => r["_field"] == "value")
            |> filter(fn: (r) => r["complex"] == "parkavewest")
            |> group(columns: ["complex"])
            |> last()
            |> drop(columns: ["_value"])
            |> rename(columns: {_time: "_value"})
            |> yield(name: "last")
        `;

        const results = await influxApi.collectRows(query)
        // console.log(results)

        const audit = results.reduce((acc, val) => {
          const { _value, result } = val;
          acc[result] = _value;
          return acc;
        }, {})

        setAudit(audit);
      } catch (err) {
        console.error('fetch data err', err);
        setError(err)
      }
    }

    fetchData();
  }, []);

  return (
    <Header error={error} audit={audit} />
  );
};

function mapStateToProps(state, props) {
  return {
    ...state.unitReducer,
    ...props,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    unitActions: bindActionCreators(unitActions, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(HeaderContainer);
