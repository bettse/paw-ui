import React, { useState, useEffect } from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {flux} from '@influxdata/influxdb-client';

import useLocalStorage from './useLocalStorage';
import * as unitActions from './actions/unitActions';
import {influxApi} from './utils';
import FloorBreakdown from './FloorBreakdown';

const {
  REACT_APP_INFLUXDB_V2_BUCKET,
} = process.env;

const FloorBreakdownContainer = props => {
  const [error, setError] = useState(null);
  const [breakdown, setBreakdown] = useLocalStorage('floorbreakdown', null);

  useEffect(() =>  {
    async function fetchData() {
      try {
        const query = flux`
          import "strings"
          from(bucket: "${REACT_APP_INFLUXDB_V2_BUCKET}")
            |> range(start: -1d)
            |> filter(fn: (r) => r["_measurement"] == "rent")
            |> filter(fn: (r) => r["_field"] == "value")
            |> filter(fn: (r) => r["complex"] == "parkavewest")
            |> filter(fn: (r) => r["scraper"] == "netlify")
            |> drop(columns: ["beds", "study"])
            |> unique(column: "unitNumber")
            |> aggregateWindow(every: 2d, fn: last, createEmpty: false)
            |> map(fn: (r) => ({
              r with
              floor: int(v: r["unitNumber"]) / 100
              }))
            |> group(columns: ["floor"])
            |> count()
            |> group(columns: ["complex"])
        `;

        const results = await influxApi.collectRows(query)
        // console.log({results})
        results.sort((a, b) => b._value - a._value)

        const top10 = results.slice(0, 9);
        const remainder = results.slice(9).reduce((sum, record) => sum += record._value, 0)

        const breakdown = top10.reduce((acc, val) => {
          const { _value, floor } = val;
          acc[floor] = _value
          return acc;
        }, {})
        breakdown['[other]'] = remainder
        // console.log({breakdown})

        setBreakdown(breakdown);
      } catch (err) {
        console.error('fetch data err', err);
        setError(err)
      }
    }
    fetchData();
  }, []);

  if (error) return <div>Error loading history</div>;
  if (!breakdown) { return null; }

  return <FloorBreakdown breakdown={breakdown} {...props} />;
};

function mapStateToProps(state, props) {
  return {
    ...state.unitReducer,
    ...props,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    unitActions: bindActionCreators(unitActions, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(FloorBreakdownContainer);
