import React, {Component} from 'react';
import {formatDate} from './utils';

class TimeseriesTick extends Component {
  render() {
    const {x, y, payload, fill} = this.props;
    return (
      <g transform={`translate(${x},${y})`}>
        <text
          x={0}
          y={0}
          dy={16}
          textAnchor="end"
          fill={fill}
          transform="rotate(-35)">
          {formatDate(payload.value)}
        </text>
      </g>
    );
  }
}

export default TimeseriesTick;
