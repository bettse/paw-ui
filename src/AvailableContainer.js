import React, {useState, useEffect} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {flux} from '@influxdata/influxdb-client';

import useLocalStorage from './useLocalStorage';
import * as unitActions from './actions/unitActions';
import {influxApi} from './utils';
import Available from './Available';

const {REACT_APP_INFLUXDB_V2_BUCKET} = process.env;

const DECIMAL = 10;
const epsilon = 60 * 1000;

const AvailableContainer = props => {
  const [error, setError] = useState(null);
  const [units, setUnits] = useLocalStorage('available-units', null);
  const [streaks, setStreaks] = useLocalStorage('available-streaks', null);

  useEffect(() => {
    async function fetchStreaks() {
      try {
        const query = flux`
          import "experimental"
          from(bucket: "${REACT_APP_INFLUXDB_V2_BUCKET}")
            |> range(start: -1y)
            |> filter(fn: (r) => r["_measurement"] == "rent")
            |> filter(fn: (r) => r["_field"] == "value")
            |> filter(fn: (r) => r["complex"] == "parkavewest")
            |> drop(columns: ["sqft", "beds", "baths", "floorplan", "study", "balcony", "scraper"])
            |> aggregateWindow(every: 1d, fn: last, createEmpty: false)
            |> group(columns: ["unitNumber"])
            |> elapsed(unit: 1d)
            |> drop(columns: ["_value"])
            |> stateCount(fn: (r) => r.elapsed < 2, column: "_value")
            |> sort(columns: ["_time"], desc: false)
            |> group(columns: ["complex"])
            |> filter(fn: (r) => r["_time"] > experimental.subDuration(d: 24h, from: now()))
            |> drop(columns: ["_measurement", "_field", "complex", "table", "elapsed"])
            |> yield(name: "streak")
        `;

        const results = await influxApi.collectRows(query);
        // console.log({results})

        const streaks = results.reduce((acc, record) => {
          const {unitNumber, _value} = record;
          const unit = parseInt(unitNumber, DECIMAL);
          acc[unit] = Math.max(_value, acc[unit] || 0);
          return acc;
        }, {});

        setStreaks(streaks);
      } catch (err) {
        console.error('fetch streaks err', err);
      }
    }

    async function fetchUnits() {
      try {
        const offsetMinutes = new Date().getTimezoneOffset();
        const query = flux`
          import "math"
          from(bucket: "${REACT_APP_INFLUXDB_V2_BUCKET}")
            |> range(start: -2d)
            |> filter(fn: (r) => r["_measurement"] == "rent")
            |> filter(fn: (r) => r["_field"] == "value")
            |> filter(fn: (r) => r["complex"] == "parkavewest")
            |> filter(fn: (r) => r["scraper"] == "netlify")
            |> drop(columns: ["study"])
            |> map(fn: (r) => ({
              r with
              sqft: int(v: r.sqft),
              baths: int(v: r.baths),
              unitNumber: int(v: r.unitNumber),
              beds: int(v: r.beds),
            }))
            |> window(period: 24h, offset: ${offsetMinutes}m)
            |> reduce(
              fn: (r, accumulator) => ({
                min: math.mMin(x: r._value, y: accumulator.min),
                max: math.mMax(x: r._value, y: accumulator.max)
              }),
              identity: {min: math.mInf(sign: 1), max: math.mInf(sign: -1)}
            )
            |> group(columns: ["complex"])
            |> drop(columns: ["_measurement", "_field", "complex", "table", "result"])
        `;

        const results = await influxApi.collectRows(query);
        // console.log({results})

        const now = new Date().getTime();
        // console.log({today})

        const units = results.reduce((acc, record) => {
          const {
            unitNumber,
            _start,
            _stop,
            table,
            result,
            min,
            max,
            ...meta
          } = record;
          const today = now - _stop < epsilon;
          const details = acc[unitNumber] || {unitNumber, ...meta, price: {}};

          if (today) {
            details.price.today = {min, max};
          } else {
            details.price.yesterday = {min, max};
          }

          if (details.price.today && details.price.yesterday) {
            details.price.delta = {
              min: details.price.today.min - details.price.yesterday.min,
              max: details.price.today.max - details.price.yesterday.max,
            };
          }

          acc[unitNumber] = details;
          return acc;
        }, {});
        // console.log({units})

        setUnits(Object.values(units));
      } catch (err) {
        console.error('fetch units err', err);
        setError(err);
      }
    }

    fetchUnits();
    fetchStreaks();
  }, []);

  if (error) return <div>Error loading available</div>;
  if (!units) {
    return null;
  }

  return <Available units={units} streaks={streaks} {...props} />;
};

function mapStateToProps(state, props) {
  return {
    ...state.unitReducer,
    ...props,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    unitActions: bindActionCreators(unitActions, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AvailableContainer);
