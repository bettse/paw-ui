import React, {useState, useEffect} from 'react';
import {When} from 'react-if';

import Card from 'react-bootstrap/Card';
import Nav from 'react-bootstrap/Nav';

import UnitHistoryContainer from './UnitHistoryContainer';
import AvgHistoryContainer from './AvgHistoryContainer';
import BedBreakdownHistoryContainer from './BedBreakdownHistoryContainer';

import './Histories.css';

const views = {
  'all avg': <AvgHistoryContainer groupBy="complex" />,
  'bedroom count': <BedBreakdownHistoryContainer />,
  'bedroom avg': <AvgHistoryContainer groupBy="beds" />,
  'unit': <UnitHistoryContainer />,
};

const defaultView = Object.keys(views)[0];

function Histories(props) {
  const [view, setView] = useState(defaultView);
  const { selected } = props;
  const { unitNumber } = selected;

  useEffect(() => {
    if (unitNumber) {
      setView('unit');
    }
  }, [unitNumber]);

  return (
    <Card>
      <Card.Header>
        <Nav variant="tabs" activeKey={view} onSelect={setView}>
          {Object.keys(views).map((v, i) =>
            <Nav.Item key={i}>
              <Nav.Link eventKey={v}>{v}</Nav.Link>
            </Nav.Item>
          )}
        </Nav>
      </Card.Header>
      <Card.Body>
          {Object.keys(views).map((v, i) =>
            <When key={i} condition={view === v}>{views[v]}</When>
          )}
      </Card.Body>
    </Card>
  );
}

export default Histories;
