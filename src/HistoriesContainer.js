import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import * as unitActions from './actions/unitActions';
import Histories from './Histories';

const HistoriesContainer = props => {
  return <Histories {...props} />;
};

function mapStateToProps(state, props) {
  return {
    ...state.unitReducer,
    ...props,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    unitActions: bindActionCreators(unitActions, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(HistoriesContainer);
