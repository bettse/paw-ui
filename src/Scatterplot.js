import React, {useState} from 'react';

import Card from 'react-bootstrap/Card';
import Nav from 'react-bootstrap/Nav';

import {
  ScatterChart,
  Scatter,
  XAxis,
  YAxis,
  Tooltip,
  CartesianGrid,
  ResponsiveContainer,
  ErrorBar,
  Cell,
  LabelList,
  ReferenceArea,
} from 'recharts';
import {schemeCategory20} from 'd3-scale';

import {rentFormatter, sqftFormat, moneyFormat} from './utils';

const height = 500;

function tooltip(props) {
  const {payload} = props;
  if (payload.length === 0) {
    return null;
  }
  const unit = payload[0].payload;
  const {unitNumber, min, max, sqft, floorplan} = unit;
  const minPricePerSqft = moneyFormat(min / sqft);
  const maxPricePerSqft = moneyFormat(max / sqft);
  const style = {
    margin: 0,
    padding: 10,
    backgroundColor: 'grey',
    border: '1px solid rgb(204, 204, 204)',
  };

  return (
    <div className="recharts-default-tooltip" style={style}>
      <p className="recharts-tooltip-label">
        Unit {unitNumber} ({floorplan})
      </p>
      <p>{sqftFormat(sqft)} Sq. Ft.</p>
      <p className="desc">
        {minPricePerSqft}-{maxPricePerSqft} per sqft
      </p>
    </div>
  );
}

function Scatterplot(props) {
  const {selected, units, unitActions} = props;
  const {loadDetail} = unitActions;
  const [dataKey, setDataKey] = useState('min');

  const sqftMin = Math.min(...units.map(unit => unit.sqft)) * 0.9;
  const sqftMax = Math.max(...units.map(unit => unit.sqft)) * 1.05;
  const priceMin = Math.min(...units.map(unit => unit.min)) * 0.9;
  const priceMax = Math.max(...units.map(unit => unit.max)) * 1.05;

  units.forEach(unit => {
    if (dataKey === 'min') {
      unit.delta = [0, unit.max - unit.min];
    } else {
      unit.delta = [unit.max - unit.min, 0];
    }
  });

  // Sort so selected is last, thus 'on top' and not obstructed by other dots
  units.sort((a, b) => {
    if (selected && selected.unitNumber === a.unitNumber) {
      return 1;
    } else if (selected && selected.unitNumber === b.unitNumber) {
      return -1;
    } else {
      return 0;
    }
  });

  const unitToCell = unit => {
    const {unitNumber, beds, justListed} = unit;
    const special = selected && selected.unitNumber === unitNumber;
    const bedColor = justListed
      ? schemeCategory20[beds * 2 + 0]
      : schemeCategory20[beds * 2 + 1];
    const fill = special ? 'grey' : bedColor;
    return <Cell key={`cell-${unitNumber}`} fill={fill} stroke={bedColor} />;
  };

  const renderLabel = props => {
    const {value} = props;
    const special = selected && selected.unitNumber === value;
    if (special) {
      return value;
    }
    return null;
  };

  const referenceAreaShape = props => {
    const {xAxis, yAxis} = props;
    const {domain} = xAxis;
    const {domain: range} = yAxis;
    const [minX, maxX] = domain;
    const [minY, maxY] = range;

    const pointString = points =>
      points
        .reduce((result, entry) => {
          if (entry.x === +entry.x && entry.y === +entry.y) {
            result.push([xAxis.scale(entry.x), yAxis.scale(entry.y)]);
          }

          return result;
        }, [])
        .join(' ');

    const yEquals2xPoints = [
      {x: minX, y: Math.max(minX * 2.0, minY)},
      {x: maxX, y: maxX * 2.0},
      {x: maxX, y: Math.max(minX * 2.0, minY)},
    ];

    const yEquals3xPoints = [
      {x: minX, y: Math.max(minX * 2.0, minY)},
      {x: maxX, y: maxX * 2.0},
      {x: maxX, y: Math.min(maxX * 3.0, maxY)},
      {x: minX, y: minX * 3.0},
    ];

    const yEquals4xPoints = [
      {x: minX, y: minX * 3.0},
      {x: maxX, y: Math.min(maxX * 3.0, maxY)},
      {x: maxX, y: maxY},
      {x: minX, y: maxY},
    ];

    return (
      <g>
        <polygon
          className="recharts-polygon"
          fill="lime"
          fillOpacity={0.05}
          points={pointString(yEquals2xPoints)}
        />
        <polygon
          className="recharts-polygon"
          fill="gold"
          fillOpacity={0.05}
          points={pointString(yEquals3xPoints)}
        />
        <polygon
          className="recharts-polygon"
          fill="crimson"
          fillOpacity={0.05}
          points={pointString(yEquals4xPoints)}
        />
      </g>
    );
  };

  return (
    <Card>
      <Card.Header>
        <Nav variant="tabs" defaultActiveKey="min" onSelect={setDataKey}>
          <Nav.Item>
            <Nav.Link eventKey="min">Min</Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link eventKey="max">Max</Nav.Link>
          </Nav.Item>
        </Nav>
      </Card.Header>
      <Card.Body>
        <Card.Title>Price per sq. ft.</Card.Title>
        <ResponsiveContainer height={height}>
          <ScatterChart>
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis
              dataKey="sqft"
              domain={[sqftMin, sqftMax]}
              type="number"
              tickFormatter={sqftFormat}
            />
            <YAxis
              dataKey={dataKey}
              domain={[priceMin, priceMax]}
              type="number"
              tickFormatter={rentFormatter}
            />
            <Tooltip content={tooltip} />
            <ReferenceArea
              shape={referenceAreaShape}
              stroke="teal"
              strokeOpacity={0.3}
            />
            <Scatter
              isAnimationActive={false}
              data={units}
              onClick={({payload}) => {
                loadDetail(payload);
              }}>
              {units.map(unitToCell)}
              <LabelList
                dataKey="unitNumber"
                position="top"
                content={renderLabel}
              />
              <ErrorBar dataKey="delta" opacity={0.1} direction="y" />
            </Scatter>
          </ScatterChart>
        </ResponsiveContainer>
      </Card.Body>
    </Card>
  );
}

export default Scatterplot;
