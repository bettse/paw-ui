import React, {useState} from 'react';

import Card from 'react-bootstrap/Card';

import {
  Charts,
  ChartContainer,
  ChartRow,
  YAxis,
  AreaChart,
  styler,
  Resizable,
} from 'react-timeseries-charts';

import {schemeCategory20} from 'd3-scale';

const style = styler([
  {key: 'zero', color: schemeCategory20[0], selected: schemeCategory20[1]},
  {key: 'one', color: schemeCategory20[2], selected: schemeCategory20[3]},
  {key: 'two', color: schemeCategory20[4], selected: schemeCategory20[5]},
]);

const fortnight = 2 * 7 * 24 * 60 * 60 * 1000;
const height = 250;

function BedBreakdownHistory(props) {
  const { series } = props;
  const [highlight, setHighlight] = useState(undefined);
  const [tracker, setTracker] = useState(undefined);
  const [timerange, setTimerange] = useState(undefined);
	const max = series.max('zero') + series.max('one') + series.max('two')

	const scrub = {}
	if (tracker) {
		const index = series.bisect(tracker);
		const trackerEvent = series.at(index);
		scrub.zero = trackerEvent.get('zero')
		scrub.one = trackerEvent.get('one')
		scrub.two = trackerEvent.get('two')
		scrub.total = scrub.zero + scrub.one + scrub.two
	}

	const trackerInfoValues = [];
	if (scrub.zero) trackerInfoValues.push({label: 'Studio', value: scrub.zero})
	if (scrub.one) trackerInfoValues.push({label: '1 Bedroom', value: scrub.one})
	if (scrub.two) trackerInfoValues.push({label: '2 Bedroom', value: scrub.two})
	if (scrub.total) trackerInfoValues.push({label: 'Total', value: scrub.total})

	return (
    <>
      <Card.Title>Breakdown by bedrooms</Card.Title>
      <Resizable>
        <ChartContainer
          timeRange={timerange || series.range()}
          maxTime={series.range().end()}
          minTime={series.range().begin()}
          trackerPosition={tracker}
          onTrackerChanged={t => setTracker(t)}
          onTimeRangeChanged={tr => setTimerange(tr)}
          minDuration={fortnight}
          timeAxisAngledLabels={true}
          enablePanZoom={true}
          >
          <ChartRow
            height={height}
            trackerShowTime={false}
            trackerInfoValues={trackerInfoValues}
            trackerInfoHeight={10 + trackerInfoValues.length * 16}
            >
            <YAxis id="y" min={0} max={max} width="20" type="linear" />
            <Charts>
              <AreaChart
                axis="y"
                series={series}
                columns={{up: series.columns(), down: []}}
                style={style}
                interpolation="curveStepAfter"
                highlight={highlight}
                onHighlightChange={h => setHighlight(h)}
              />
            </Charts>
          </ChartRow>
        </ChartContainer>
      </Resizable>
    </>
  );
}


export default BedBreakdownHistory;
