import React, { useState, useEffect } from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {flux} from '@influxdata/influxdb-client';

import useLocalStorage from './useLocalStorage';
import * as unitActions from './actions/unitActions';
import {influxApi} from './utils';
import Scatterplot from './Scatterplot';

const {
  REACT_APP_INFLUXDB_V2_BUCKET,
} = process.env;

const ScatterplotContainer = (props) => {
  const [error, setError] = useState(null);
  const [units, setUnits] = useLocalStorage('scatter-units', []);

  useEffect(() =>  {
    async function fetchData() {
      try {
        const query = flux`
          import "math"
          from(bucket: "${REACT_APP_INFLUXDB_V2_BUCKET}")
          |> range(start: -1d)
          |> filter(fn: (r) => r["_measurement"] == "rent")
          |> filter(fn: (r) => r["_field"] == "value")
          |> filter(fn: (r) => r["complex"] == "parkavewest")
          |> filter(fn: (r) => r["scraper"] == "netlify")
          |> reduce(
            fn: (r, accumulator) => ({
              // r with
              min: math.mMin(x: r._value, y: accumulator.min),
              max: math.mMax(x: r._value, y: accumulator.max)
            }),
            identity: {min: math.mInf(sign: 1), max: math.mInf(sign: -1)}
          )
          |> group(columns: ["complex"])
          |> map(fn: (r) => ({
            r with
            sqft: int(v: r.sqft),
            baths: int(v: r.baths),
            unitNumber: int(v: r.unitNumber),
            beds: int(v: r.beds),
          }))
          |> drop(columns: ["_start", "_stop", "_time"])
        `;

        const results = await influxApi.collectRows(query)
        // console.log({results})

        setUnits(results);
      } catch (err) {
        console.error('fetch data err', err);
        setError(err)
      }
    }
    fetchData();
  }, []);

  if (error) return <div>Error loading history</div>;
  if (!units) { return null; }

  return <Scatterplot units={units} {...props} />;
};

function mapStateToProps(state, props) {
  return {
    ...state.unitReducer,
    ...props,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    unitActions: bindActionCreators(unitActions, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ScatterplotContainer);
