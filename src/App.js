import React, {useState} from 'react';

import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Alert from 'react-bootstrap/Alert';

import Konami from 'react-konami-code';

import AvailableContainer from './AvailableContainer';
import Breakdowns from './Breakdowns';
import HistoriesContainer from './HistoriesContainer';
import ScatterplotContainer from './ScatterplotContainer';
import HeaderContainer from './HeaderContainer';

function App(props) {
  const [notification, setNotification] = useState(false);

  const easterEgg = () => {
    setNotification(true);
    localStorage.clear();
  };

  return (
    <div>
      <Konami action={easterEgg}>
        <Alert
          variant="warning"
          show={notification}
          onClose={() => setNotification(false)}
          dismissible>
          Local storage cleared
        </Alert>
      </Konami>
      <HeaderContainer />
      <Container fluid="xl">
        <Row>
          <Col md={12} lg={6}>
            <AvailableContainer />
            <HistoriesContainer />
          </Col>
          <Col md={12} lg={6}>
            <Breakdowns />
            <ScatterplotContainer />
          </Col>
        </Row>
      </Container>
    </div>
  );
}

export default App;
