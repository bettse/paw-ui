import React, { useState, useEffect } from 'react';
import {connect} from 'react-redux';
import {flux} from '@influxdata/influxdb-client';
import {TimeSeries, TimeEvent} from 'pondjs';

import useLocalStorage from './useLocalStorage';
import {influxApi} from './utils';
import UnitHistory from './UnitHistory';

const {
  REACT_APP_INFLUXDB_V2_BUCKET,
} = process.env;

const UnitHistoryContainer = props => {
  const { selected } = props
  const { unitNumber } = selected;
  if (!unitNumber) {
    return 'Select a unit on the lefthand table to see its rent history';
  }

  const [error, setError] = useState(null);
  const [rawSeries, setRawSeries] = useLocalStorage(`unitHistory-rawSeries-${unitNumber}`, null);
  const [series, setSeries] = useState(null);
  const [streak , setStreak] = useLocalStorage(`unitHistory-streak-${unitNumber}`, null);
  const [highest, setHighest] = useLocalStorage(`unitHistory-highest-${unitNumber}`, null);
  const [lowest, setLowest] = useLocalStorage(`unitHistory-lowest-${unitNumber}`, null);

  useEffect(() =>  {
    async function fetchLowest() {
      try {
        const query = flux`
          from(bucket: "${REACT_APP_INFLUXDB_V2_BUCKET}")
          |> range(start: -10y)
          |> filter(fn: (r) => r["_measurement"] == "rent")
          |> filter(fn: (r) => r["_field"] == "value")
          |> filter(fn: (r) => r["unitNumber"] == "${unitNumber}")
          |> lowestMin(n:1)
          |> yield(name: "lowest")
        `;

        const results = await influxApi.collectRows(query)
        // console.log({results})

        const [result] = results;
        setLowest(result._value);
      } catch (err) {
        console.error('fetch lowest err', err);
      }
    }

    async function fetchHighest() {
      try {
        const query = flux`
          from(bucket: "${REACT_APP_INFLUXDB_V2_BUCKET}")
          |> range(start: -10y)
          |> filter(fn: (r) => r["_measurement"] == "rent")
          |> filter(fn: (r) => r["_field"] == "value")
          |> filter(fn: (r) => r["unitNumber"] == "${unitNumber}")
          |> highestMax(n:1)
          |> yield(name: "highest")
        `;

        const results = await influxApi.collectRows(query)
        // console.log({results})

        const [result] = results;
        setHighest(result._value);
      } catch (err) {
        console.error('fetch highest err', err);
      }
    }

    async function fetchStreak() {
      try {
        const query = flux`
          from(bucket: "${REACT_APP_INFLUXDB_V2_BUCKET}")
          |> range(start: -12mo)
          |> filter(fn: (r) => r["_measurement"] == "rent")
          |> filter(fn: (r) => r["_field"] == "value")
          |> filter(fn: (r) => r["unitNumber"] == "${unitNumber}")
          |> drop(columns: ["sqft", "beds", "baths", "floorplan", "study", "balcony", "scraper"])
          |> aggregateWindow(every: 1d, fn: mean, createEmpty: false)
          |> drop(columns: ["_value"])
          |> elapsed(unit: 1d)
          |> stateCount(fn: (r) => r.elapsed < 2, column: "_value")
          |> sort(columns: ["_time"], desc: false)
          |> last()
          |> drop(columns: ["_measurement", "_field", "complex", "table", "elapsed"])
          |> yield(name: "streak")
        `;

        const results = await influxApi.collectRows(query)
        // console.log({results})

        const [result] = results;
        if (result) {
          setStreak(Math.max(result._value, 0));
        } else {
          setStreak(0);
        }
      } catch (err) {
        console.error('fetch streak err', err);
      }
    }

    async function fetchRawSeries() {
      if (rawSeries) {
        const lastMax = rawSeries[rawSeries.length - 1];
        const lastDay = new Date(lastMax._time)
          .toISOString()
          .split('T')
          .shift();
        const today = new Date()
          .toISOString()
          .split('T')
          .shift();
        if (lastDay === today) {
          // TODO: Handle that the rawSeries could be for the previous unitNumber
          // console.log('Cache has up to date history, not requesting new data')
          // return;
        }
      }
      try {
        const query = flux`
          from(bucket: "${REACT_APP_INFLUXDB_V2_BUCKET}")
          |> range(start: -1y)
          |> filter(fn: (r) => r["_measurement"] == "rent")
          |> filter(fn: (r) => r["_field"] == "value")
          |> filter(fn: (r) => r["unitNumber"] == "${unitNumber}")
          |> group(columns: ["unitNumber"])
          |> aggregateWindow(every: 1d, fn: min)
          |> yield(name: "min")

          from(bucket: "${REACT_APP_INFLUXDB_V2_BUCKET}")
          |> range(start: -1y)
          |> filter(fn: (r) => r["_measurement"] == "rent")
          |> filter(fn: (r) => r["_field"] == "value")
          |> filter(fn: (r) => r["unitNumber"] == "${unitNumber}")
          |> group(columns: ["unitNumber"])
          |> aggregateWindow(every: 1d, fn: max)
          |> yield(name: "max")
        `;

        const results = await influxApi.collectRows(query)
        // console.log({results})

        setRawSeries(results);
      } catch (err) {
        console.error('fetch data err', err);
        setError(err)
      }
    }
    if (unitNumber){
      fetchRawSeries();
      fetchStreak();
      fetchHighest();
      fetchLowest();
    }
  }, [unitNumber]);

  useEffect(() =>  {
    if (rawSeries === null) {
      return;
    }

    // rawSeries have rows for min, followed by rows for max, so we corrolate them based on time
    const joined = rawSeries.reduce((acc, val) => {
      const { _time, _value, result } = val;
      acc[_time] = Object.assign(acc[_time] || {}, {time: _time, [result]: _value})
      return acc;
    }, {})

    const events = Object.values(joined).map(record => {
      const { time, ...rest } = record;
      return new TimeEvent(time, rest);
    });
    events.sort((a, b) => a.key() - b.key())

    const series = new TimeSeries({
      name: "unitHistory",
      events
    });
    // console.log(series.toJSON())

    setSeries(series)
  }, [rawSeries])

  if (error) return <div>Error loading history</div>;
  if (!series) { return null; }

  return <UnitHistory
    number={unitNumber}
    selected={selected}
    series={series}
    streak={streak}
    highest={highest}
    lowest={lowest}
  />;
};

function mapStateToProps(state, props) {
  return {
    ...state.unitReducer,
    ...props,
  };
}

function mapDispatchToProps(dispatch) {
  return {};
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(UnitHistoryContainer);
