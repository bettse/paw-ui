import React, {useState} from 'react';
import {When} from 'react-if';

import Card from 'react-bootstrap/Card';
import Nav from 'react-bootstrap/Nav';

import BedBreakdownContainer from './BedBreakdownContainer';
import FloorplanBreakdownContainer from './FloorplanBreakdownContainer';
import FloorBreakdownContainer from './FloorBreakdownContainer';

import './Breakdowns.css';

const views = {
  'bedrooms': <BedBreakdownContainer />,
  'floorplans': <FloorplanBreakdownContainer />,
  'floors': <FloorBreakdownContainer />,
};

const defaultView = 'bedrooms';

function Breakdowns(props) {
  const [view, setView] = useState(defaultView);

  return (
    <Card>
      <Card.Header>
        <Nav variant="tabs" defaultActiveKey={defaultView} onSelect={setView}>
          {Object.keys(views).map((v, i) =>
            <Nav.Item key={i}>
              <Nav.Link eventKey={v}>{v}</Nav.Link>
            </Nav.Item>
          )}
        </Nav>
      </Card.Header>
      <Card.Body>
          {Object.keys(views).map((v, i) =>
            <When key={i} condition={view === v}>{views[v]}</When>
          )}
      </Card.Body>
    </Card>
  );
}

export default Breakdowns;
